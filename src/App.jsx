import "./App.css";
import Header from "./components/Header";
import Home from "./components/Home";
import About from "./components/About";
import Projects from "./components/Projects";
import Socials from "./components/Socials";
import Footer from "./shared/Footer";
import { useDarkMode } from "./shared/DarkMode";

function App() {
  const { darkMode } = useDarkMode();
  return (
    <>
      <div className={`${darkMode ? "dark bg-black" : ""}`}>
      <Header/>
      <Home/>
      <About/>
      <Projects/>
      <Socials/>
      <Footer/>
      </div>
    </>
  );
}

export default App;
