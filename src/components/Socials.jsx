/* eslint-disable no-unused-vars */
import React from 'react';
import Headline from '../shared/Headline';
import { motion } from 'framer-motion';
import { fadeIn } from '../variants';
import { useDarkMode } from "../shared/DarkMode";

const Socials = () => {
    const { darkMode } = useDarkMode();
    return (
        <div className={darkMode ? "dark" : ""}>
        <motion.div className='dark:bg-black dark:text-white contact mx-auto mt-8 px-7' id="socials"
            variants={fadeIn("up", 0.2)}
            initial="hidden"
            whileInView={"show"}
            viewport={{once:false, amount: 0.2}}
        >
            <Headline title="SOCIALS"/>
            <div className='md:w-2/3 mx-auto md:px-16 px-8 py-8 rounded mb-32 text-center'>
                <div className='flex justify-center'>
                <a href="https://www.linkedin.com/in/danalelchuk/" className='p-3 block rounded transition-all duration-500 project-icon' title="visit my linkedin">
                    <svg xmlns="http://www.w3.org/2000/svg" className={`w-10 h-10 m-5 ${darkMode ? 'dark-fill-white' : ''}`} viewBox="0 0 512 512">
                        <title>Logo Linkedin</title>
                        <path d="M444.17 32H70.28C49.85 32 32 46.7 32 66.89v374.72C32 461.91 49.85 480 70.28 480h373.78c20.54 0 35.94-18.21 35.94-38.39V66.89C480.12 46.7 464.6 32 444.17 32zm-273.3 373.43h-64.18V205.88h64.18zM141 175.54h-.46c-20.54 0-33.84-15.29-33.84-34.43 0-19.49 13.65-34.42 34.65-34.42s33.85 14.82 34.31 34.42c-.01 19.14-13.31 34.43-34.66 34.43zm264.43 229.89h-64.18V296.32c0-26.14-9.34-44-32.56-44-17.74 0-28.24 12-32.91 23.69-1.75 4.2-2.22 9.92-2.22 15.76v113.66h-64.18V205.88h64.18v27.77c9.34-13.3 23.93-32.44 57.88-32.44 42.13 0 74 27.77 74 87.64z" fill={darkMode ? 'white' : 'black'} />
                    </svg>
                </a>
                <a href="https://gitlab.com/lelchukdana" className='p-3 block rounded transition-all duration-500 project-icon' title="visit my GitLab">
                    <svg xmlns="http://www.w3.org/2000/svg" className={`w-10 h-10 m-5 ${darkMode ? 'dark-fill-white' : ''}`} viewBox="0 0 512 512">
                        <title>Logo Gitlab</title>
                        <path d="M494.07,281.6l-25.18-78.08a11,11,0,0,0-.61-2.1L417.78,44.48a20.08,20.08,0,0,0-19.17-13.82A19.77,19.77,0,0,0,379.66,44.6L331.52,194.15h-152L131.34,44.59a19.76,19.76,0,0,0-18.86-13.94h-.11a20.15,20.15,0,0,0-19.12,14L42.7,201.73c0,.14-.11.26-.16.4L16.91,281.61a29.15,29.15,0,0,0,10.44,32.46L248.79,476.48a11.25,11.25,0,0,0,13.38-.07L483.65,314.07a29.13,29.13,0,0,0,10.42-32.47m-331-64.51L224.8,408.85,76.63,217.09m209.64,191.8,59.19-183.84,2.55-8h86.52L300.47,390.44M398.8,59.31l43.37,134.83H355.35M324.16,217l-43,133.58L255.5,430.14,186.94,217M112.27,59.31l43.46,134.83H69M40.68,295.58a6.19,6.19,0,0,1-2.21-6.9l19-59L197.08,410.27M470.34,295.58,313.92,410.22l.52-.69L453.5,229.64l19,59a6.2,6.2,0,0,1-2.19,6.92" fill={darkMode ? 'white' : 'black'} />
                    </svg>
                </a>
                </div>
            </div>
        </motion.div>
        </div>
    );
};

export default Socials;
