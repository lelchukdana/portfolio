
const projects = [
    {
        id: 1,
        name: 'Bingeworthy',
        description: 'Bingeworthy is a streaming project that I collaborated on which allows users to find streaming providers, explore, rate, review, comment, and save TV shows and Movies to a watchlist',
        image: '/src/assets/BingeworthyLaptop.png',
        imageDarkMode: '/src/assets/BingeworthyLaptop.png',
        link:'https://gitlab.com/team-13-pending/bingeworthy',

    },
    {
        id: 2,
        name: 'CarCar',
        description: 'CarCar is a an application managing all aspects of an automobile dealership, including inventory, service and sales.',
        image: '/src/assets/CarCarLaptop.gif',
        imageDarkMode: '/src/assets/CarCarLaptopBlackBG.gif',
        link:'https://gitlab.com/_Erick.33/project-beta'
    },
    {
        id: 3,
        name: 'RainOrShine',
        description: 'RainOrShine is a an application which allows users to view weather data for any city',
        image: '/src/assets/RainOrShineLaptop.png',
        imageDarkMode: '/src/assets/RainOrShineLaptop.png',
        link:'https://gitlab.com/lelchukdana/rainorshine'
    },
    {
        id: 4,
        name: 'Coming Soon',
        description: 'I am always working on a project on the side. You can take a sneak peak at my Gitlab, or just be sure to check out this section soon for updates!',
        image: '/src/assets/FutureProjectsLaptop.png',
        imageDarkMode: '/src/assets/FutureProjectsLaptopBlack.png',
        link:'https://gitlab.com/lelchukdana'
    },
]


export default projects;
